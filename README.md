# Introduction
This repository simply demonstrates that it is possible to use conan in a
catkin workspace.

# How it's done
[cmake-conan](https://github.com/conan-io/cmake-conan) is used to activate
conan during the cmake build process to install and find the dependencies.

Building with conan requires to define the build type:
``` bash
catkin_make_isolated --cmake-args -DCMAKE_BUILD_TYPE=Release
```
or
``` bash
catkin build --cmake-args -DCMAKE_BUILD_TYPE=Release
```
Also note that the workspace will be a mixed catkin and cmake workspace. So only
`catkin_make_isolated` and `catkin build` work.
Since catkin libraries will be built as position independent shared libraries by 
default, use the `*:shared=True` and `*.fPIC=True` options in conanfile.txt.

# Using cmake_find_package generator
This is what is shown here. The cmake_find_package generator to generate the
findXXX.cmake files. Conan does not modify the CMAKE_PREFIX_PATH which would
disturb the catkin build process. Those targets can be found via find_package() 
and later be linked with the built targets.

# Using cmake_paths generator
Works much like cmake_find_package. Include the generated cmake file but don't
call the `conan_basic_setup`.

# Using cmake generator
This is generally not advised, since catkin heavily depends on the 
CMAKE_PREFIX_PATH which is modified by the `conan_basic_setup()`. It is
possible to build the project, if the conan related stuff is placed between
`catkin_package` and the targets, but the binaries won't find it's way to
the devel space.

# Requirements
Setup the following conan repositories:
``` bash
conan remote add tuebel-gl3w https://api.bintray.com/conan/tuebel/gl3w
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

# Using conan packages across catkin packages
Since catkin heavily modifies the cmake paths, it will not find conan packages
referenced in catkin dependencies.
A simple solution is to use a conanfile.txt in every package and pull in the same
dependencies.
Keep in mind that different versions might lead to incompatible libs.
