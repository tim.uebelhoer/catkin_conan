#include <use_gl3w_ros/use_gl3w_ros.hpp>

namespace use_gl3w_ros
{
int UseGl3wRos::test()
{
  return gl_ros.test();
}
} // namespace gl3w_ros